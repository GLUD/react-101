import React from 'react'
import './index.css'
import zorrito from  '../../assets/logo192.png'


const Card = ({textPrincipal,secondaryText, text3}) => {
    return(
        <div className="container">
            <p>{textPrincipal}</p>
            {secondaryText ? (
                <p>{secondaryText}</p>
            ): null}
            {text3 ? (
                <p>{text3}</p>
            ) : (
                <img src={zorrito} alt="" />
            ) }
        </div>
    )
}

export default Card