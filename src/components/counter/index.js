import React, { useState } from 'react'
import './index.css'

const Counter = ({initialCount}) => {
    const [count, setCount] = useState(initialCount)
    const [formData, setFormData] = useState({
        name: '',
        email: '',
        passwd: ''
    })

    const handleChange = (e) =>{
        e.preventDefault()
        setFormData({...formData, [e.target.name]: e.target.value})
    }
    return(
        <div className="flex-container">
            <span>{count}</span>
            <div className="flex-row-container">
                <button onClick={()=> setCount(count+1)}>+</button>
                <button onClick={()=> setCount(count-1)}>-</button>
                <button onClick={()=> setCount(initialCount)}>Reset</button>
            </div>
            <form onSubmit={(e)=>{
                e.preventDefault()
                console.log(formData)
                }}>
                <input onChange={handleChange} type="text" name='name' placeholder="Nombre" required/>
                <input onChange={handleChange} type="email" name="email" placeholder="Correo Electrónico" required/>
                <input onChange={handleChange} type="password" name="passwd" placeholder="Contraseña" required/>
                <button type="submit">Enviar</button>
            </form>
        </div>
    )
}

export default Counter